/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajero;

/**
 *
 * @author javier
 */
public class Pila {
   
    class Nodo {
        int info;
        Nodo sig;
    }
    int longitud;
    private Nodo raiz;
    
    public Pila () {
        raiz=null;
        longitud=0;
    }
    public Pila (int elementos[]) {
        raiz=null;
        longitud=0;
        for(int i =elementos.length; i >0; i--){
            insertar(elementos[i-1]);
        }
    }
    
    public void insertar(int x) {
    	Nodo nuevo;
        nuevo = new Nodo();
        nuevo.info = x;
        if (raiz==null)
        {
            nuevo.sig = null;
            raiz = nuevo;
        }
        else
        {
            nuevo.sig = raiz;
            raiz = nuevo;
        }
        longitud++;
    }
    
    public int extraer ()
    {
        if (raiz!=null)
        {
            int informacion = raiz.info;
            raiz = raiz.sig;
            longitud--;
            return informacion;
        }
        else
        {
            return Integer.MAX_VALUE;
        }
    }
    
    public int ultimo ()
    {
        if (raiz!=null)
        {
            int informacion = raiz.info;
            return informacion;
        }
        else
        {
            return Integer.MAX_VALUE;
        }
    }
    
    
    public void imprimir() {
        Nodo reco=raiz;
        System.out.println("Listado de todos los elementos de la pila.");
        while (reco!=null) {
            System.out.print(reco.info+"-");
            reco=reco.sig;
        }
        System.out.println();
    }
    
    public int[] elementos() {
        int elementos[] = new int[longitud];
        Nodo reco=raiz;
        int indice =0;
        while (reco!=null) {
            elementos[indice]=reco.info;
            indice++;
            reco=reco.sig;
        }
        return elementos;
    }
    
    public String toString() {
        Nodo reco=raiz;
        System.out.println("Listado de todos los elementos de la pila.");
        while (reco!=null) {
            System.out.print(reco.info+"-");
            reco=reco.sig;
        }
        System.out.println();
        return "imprimiendo";
    }
    
}
